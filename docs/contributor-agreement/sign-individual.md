# Sign the Individual CLA

**Ready to sign the CLA+DCO?** Press one of the links below to get started.

[Sign the Individual CLA now :octicons-mark-github-16:](https://rtapp.tk/oss-cla-individual-gh){ .md-button .md-button--primary }  
[Using GitLab? :fontawesome-brands-gitlab:](https://rtapp.tk/oss-cla-individual-gl){ .md-button }

## Instructions

1. Use `Sign the CLA` issue template for GitHub or `CLA-Sign` for GitLab.
2. In the issue title, use the template below


```
[@rtapp/cla-sign] Signing [gh|gl]:UsernameGoesHere as an individual
```

   * For GitHub users, it should be `[@rtapp/cla-sign] Signing gh:username as an invidual`.
   * For GitLab users, it should be `[@rtapp/cla-sign] Signing gl:username as an individual`.

3. The rest of the issue description are some text humans wrote so bots can detect that you want to sign our unified DCO-CLA. Just hit `Create new issue` to trigger the CI
4. Wait for our robots to respond, this may take up to a minute, depending if we got rate-limited. Once responsed, follow the instructions to sign the CLA (if you completely new to open-source community, this is your first merge request, probably).

## FAQs

See <https://github.com/MadeByThePinsHub/contributor-agreement#faqs> for the full FAQ.

## Have questions?

Before you ask, we remind you that we're no lawyers, we're just internet people. If you have legal inquiries about CLAs, copyright, and even DCO, please consult your attorney for legal advice.

* Ask us questions on [the Discussions tab on GitHub](https://github.com/MadeByThePinsHub/contributor-agreement/discussions).
* You can also contact us directly [using these information here](https://madebythepins.tk/contact), just make sure you had read the FAQs.
