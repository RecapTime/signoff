# Contributor License Agreement

## Getting Started

To get started on our CLA,

* [Read the full unified CLA](./view) first.
* Then sign as either an [individual](sign-individual) or [entity](sign-entity).
* Volia! You're done.

Have any questions? Let us know in [GitHub Discussions](https://github.com/MadeByThePinsHub/contributor-agreement/discussions) or
see the [frequent asked questions](https://github.com/MadeByThePinsHub/contributor-agreement#faqs).

## Managing your signatures

We're working on an API and Web UI for managing your signatures very soon.
